== README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version ruby 2.1.2p95 (2014-05-08) [x86_64-linux-gnu]

* System dependencies (is in Gemfile)

* Configuration

* Database creation

* Database initialization ggerman.sql OR rake db:migrate

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.
