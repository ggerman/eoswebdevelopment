class StaticPagesController < ApplicationController
  def show_section
    @cms = Cms.where(cms_id: params["section_name"]).first
  end
end
