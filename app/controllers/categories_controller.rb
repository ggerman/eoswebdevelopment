class CategoriesController < ApplicationController
	def create
		@category = Category.new(category_params)

	end

	def show

		@category = Category.find params[:id]
		@posts =  @category.posts
	end

private
    def category_params
      params.require(:category).permit(:title, :category_ids)
    end



end
