class Post < ActiveRecord::Base
	has_many :comments

	belongs_to :category
	belongs_to :section

	validates :title, presence: true,
		    length: {minimum: 5}
	has_attached_file :asset, :styles => { :detailed => "1920x1920>", :thumb => "100x100>" }
	validates_attachment_content_type :asset, :content_type => /\Aimage\/.*\Z/
end
