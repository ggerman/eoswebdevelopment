class AddSections < ActiveRecord::Migration
  def change
	create_table :sections do |t|
		t.column :title, :string, :null => false
	end
	change_table :posts do |t|
		t.references :section
	end
  end
end
