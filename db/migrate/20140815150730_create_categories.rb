class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
	t.column :title, :string, :null => false
	t.column :slug, :string, :null => false
    end

	change_table :posts do |t|
		t.references :category

	end
  end



end
