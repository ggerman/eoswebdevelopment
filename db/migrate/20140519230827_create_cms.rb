class CreateCms < ActiveRecord::Migration
  def change
    create_table :cms do |t|
      t.string :title
      t.string :image
      t.text :content

      t.timestamps
    end
  end
end
